import random
import string
from datetime import datetime, timedelta


def generate_full_name(starting_letter=None):
    first_name = ''.join(random.choice(string.ascii_uppercase) +
                         ''.join(random.sample(string.ascii_lowercase, random.randint(4, 10))))
    last_name = ''.join(random.choice(string.ascii_uppercase) +
                        ''.join(random.sample(string.ascii_lowercase, random.randint(4, 10))))
    patronymic = ''.join(random.choice(string.ascii_uppercase) +
                         ''.join(random.sample(string.ascii_lowercase, random.randint(4, 10))))
    if starting_letter is not None:
        last_name = starting_letter + last_name[1:]

    return f"{last_name} {first_name} {patronymic}"


def generate_random_employee(starting_letter=None):
    full_name = generate_full_name(starting_letter)
    birth_date = datetime.today() - timedelta(days=random.randint(18 * 365, 60 * 365))
    gender = random.choice(["Male", "Female"])

    return {
             "full_name": full_name,
             "birth_date": birth_date,
             "gender": gender
    }
