from datetime import datetime
from db.connection import SessionLocal
from db.models import EmployeeModel


class Employee:
    def __init__(self, full_name, birth_date, gender):
        self.full_name = full_name
        self.birth_date = datetime.strptime(birth_date, "%Y-%m-%d") \
            if isinstance(birth_date, str) else birth_date
        self.gender = gender
        self.age = self.calculate_age()

    def __repr__(self):
        return (f"Employee '{self.full_name}' with birthdate: "
                f"{self.birth_date}, gender: {self.gender}, "
                f"and complete years: {self.age}")

    def calculate_age(self):
        today = datetime.today()
        age = (today.year - self.birth_date.year -
               ((today.month, today.day) < (self.birth_date.month, self.birth_date.day)))
        return age

    def save_to_database(self):
        session = SessionLocal()
        try:
            employee_model = EmployeeModel(
                full_name=self.full_name,
                birth_date=self.birth_date,
                gender=self.gender
            )
            session.add(employee_model)
            session.commit()
            print(f"Employee {self.full_name} successfully added. Complete years: {self.age}")
        except Exception as e:
            print(e)
        finally:
            session.close()

    @staticmethod
    def bulk_create(session, employees):
        session.bulk_save_objects(employees)
        session.commit()
        session.close()
