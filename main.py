import sys

from db.utils import create_tables, query_employees, insert_employees, find_employees, optimized_finder
from utils.employee import Employee


def main():
    if sys.argv[1] == '1':
        create_tables()
    elif sys.argv[1] == '2':
        employee = Employee(sys.argv[2], sys.argv[3], sys.argv[4])
        employee.save_to_database()
    elif sys.argv[1] == '3':
        query_employees()
    elif sys.argv[1] == '4':
        insert_employees(num_records=1000000)
        insert_employees(num_records=100, starting_letter='F')
        print("1.000.000 and 100 records successfully added.")
    elif sys.argv[1] == '5':
        employees, exec_time = find_employees()
        print("Amount Employees with Male gender and last name starting with 'F':")
        print(len(employees))
        print(f"Execution time: {exec_time} seconds")
    elif sys.argv[1] == '6':
        employees, exec_time = find_employees()
        employees_optimized, exec_time_optimized = optimized_finder()
        print("Amount Employees with Male gender and last name starting with 'F':")
        print(len(employees))
        print(f"Execution time: {exec_time} seconds")
        print("Amount optimized Employees:")
        print(len(employees_optimized))
        print(f"Execution optimized time: {exec_time_optimized} seconds")


if __name__ == '__main__':
    main()
