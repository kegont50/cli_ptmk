from sqlalchemy import Column, Integer, String, Date

from .connection import Base


class EmployeeModel(Base):
    __tablename__ = "employees"

    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String)
    birth_date = Column(Date)
    gender = Column(String)
