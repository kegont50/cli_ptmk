import time

from sqlalchemy import text

from utils.data_generate import generate_random_employee
from utils.employee import Employee
from .connection import engine, SessionLocal
from .models import Base, EmployeeModel


def create_tables():
    Base.metadata.create_all(bind=engine)
    print("The table was successfully created")


def query_employees():
    session = SessionLocal()
    query = session.query(
        EmployeeModel.full_name,
        EmployeeModel.birth_date,
        EmployeeModel.gender
    ).group_by(EmployeeModel.full_name, EmployeeModel.birth_date)

    unique_employees = query.all()

    for unique_employee in unique_employees:
        print(create_employee(*unique_employee))

    session.close()


def create_employee(*args):
    return Employee(*args)


def employee_model(**kwargs):
    return EmployeeModel(**kwargs)


def insert_employees(num_records: int, starting_letter: str = None):
    session = SessionLocal()
    employees = [employee_model(**generate_random_employee(starting_letter)) for _ in range(num_records)]
    Employee.bulk_create(session=session, employees=employees)


def find_employees():
    session = SessionLocal()
    start_time = time.time()

    query = session.query(EmployeeModel).filter(
        EmployeeModel.full_name.like("F%"),
        EmployeeModel.gender == "Male"
    )
    employees = query.all()

    end_time = time.time()
    exec_time = end_time - start_time

    return employees, exec_time


def optimized_finder():
    sql_query = text("""
        SELECT *
        FROM employees
        WHERE full_name LIKE 'F%'
          AND gender = 'Male';
    """)

    session = SessionLocal()
    start_time = time.time()
    result = session.execute(sql_query).all()
    end_time = time.time()
    exec_time = end_time - start_time

    return result, exec_time
